`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/03 09:12:44
// Design Name:
// Module Name: position
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module position(clk,
                resetb,
                ie,
                hcnt,
                vcnt,
                idat,
                oe,
                x_max,
                x_min,
                y_max,
                y_min

               );
parameter ck2q = 1;
input clk;
input resetb;
input ie;
input idat;
input [10: 0] hcnt;
input [10: 0] vcnt;
output oe;
output [10: 0] x_max;
output [10: 0] x_min;
output [10: 0] y_max;
output [10: 0] y_min;
wire comp_tc = (hcnt == 11'd0) & (vcnt == 11'd0);
reg [10: 0] x_maxr;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			x_maxr <= #ck2q 215;
		else if (comp_tc)
			x_maxr <= #ck2q 215;
		else if (ie)
			begin
				if ((idat) & (x_maxr <= hcnt))
					x_maxr <= #ck2q hcnt;
			end
	end
reg [10: 0] x_minr;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			x_minr <= #ck2q 11'd1015;
		else if (comp_tc)
			x_minr <= #ck2q 11'd1015;
		else if (ie)
			begin
				if ((idat) & (x_minr > hcnt))
					x_minr <= #ck2q hcnt;
			end
	end
reg [10: 0] y_maxr;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			y_maxr <= #ck2q 26;
		else if (comp_tc)
			y_maxr <= #ck2q 26;
		else if (ie)
			begin
				if ((idat) & (y_maxr <= vcnt))
					y_maxr <= #ck2q vcnt;
			end
	end
reg [10: 0] y_minr;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			y_minr <= #ck2q 11'd626;
		else if (comp_tc)
			y_minr <= #ck2q 11'd626;
		else if (ie)
			begin
				if ((idat) & (y_minr > vcnt))
					y_minr <= #ck2q vcnt;
			end
	end
reg [10: 0] x_max, y_max, x_min, y_min;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			begin
				x_max <= #ck2q 0;
				y_max <= #ck2q 0;
				x_min <= #ck2q 0;
				y_min <= #ck2q 0;
			end
		else if (comp_tc)
			begin
				x_max <= #ck2q x_maxr;
				y_max <= #ck2q y_maxr;
				x_min <= #ck2q x_minr;
				y_min <= #ck2q y_minr;
			end
	end
reg oe;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			oe <= #ck2q 0;
		else
			oe <= #ck2q comp_tc;
	end
endmodule
