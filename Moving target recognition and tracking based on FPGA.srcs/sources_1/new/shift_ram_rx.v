`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/02 20:32:05
// Design Name:
// Module Name: shift_ram_rx
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


// `timescale 1ns/1ns
module shift_ram_rx(
           clk,
           resetb,
           ie,
           idat,
           oe,
           odat0,
           odat1,
           odat2
       );
parameter DW = 16;
parameter AW = 10;
parameter DP = 10'd640;
parameter ck2q = 1;
input clk;
input resetb;
input ie;
input [DW - 1: 0] idat;
output oe;
output [DW - 1: 0] odat0;
output [DW - 1: 0] odat1;
output [DW - 1: 0] odat2;
//
///////////////////////////////////////////////////////////////////////////////
//
//================= line 0 ====================
reg [AW - 1: 0] waddr0;
wire [AW - 1: 0] waddr0_p1 = waddr0 + 1'b1;
wire waddr0_tc = (waddr0_p1 == DP);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			waddr0 <= #ck2q 0;
		else if (ie)
			begin
				if (waddr0_tc)
					waddr0 <= #ck2q 0;
				else
					waddr0 <= #ck2q waddr0_p1;
			end
	end
reg full0_ed;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			full0_ed <= #ck2q 0;
		else if (waddr0_tc)
			full0_ed <= #ck2q 1'b1;
	end
wire rd0 = ie ? full0_ed : 1'b0;
reg [AW - 1: 0] raddr0;
wire [AW - 1: 0] raddr0_p1 = raddr0 + 1'b1;
wire raddr0_tc = (raddr0_p1 == DP);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			raddr0 <= #ck2q 0;
		else if (rd0)
			begin
				if (raddr0_tc)
					raddr0 <= #ck2q 0;
				else
					raddr0 <= #ck2q raddr0_p1;
			end
	end
wire [DW - 1: 0] rdat0;
dpram u_dpram_0(
          .wr ( ie ),
          .wclk ( clk ),
          .wdat ( idat ),
          .waddr ( waddr0 ),
          .rd ( rd0 ),
          .rclk ( clk ),
          .rdat ( rdat0 ),
          .raddr ( raddr0 )
      );
//================= line 1 ====================
reg rd0_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			rd0_d0 <= #ck2q 0;
		else
			rd0_d0 <= #ck2q rd0;
	end
reg [AW - 1: 0] waddr1;
wire [AW - 1: 0] waddr1_p1 = waddr1 + 1'b1;
wire waddr1_tc = (waddr1_p1 == DP);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			waddr1 <= #ck2q 0;
		else if (rd0_d0)
			begin
				if (waddr1_tc)
					waddr1 <= #ck2q 0;
				else
					waddr1 <= #ck2q waddr1_p1;
			end
	end
reg full1_ed;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			full1_ed <= #ck2q 0;
		else if (waddr1_tc)
			full1_ed <= #ck2q 1'b1;
	end
wire rd1 = ie ? full1_ed : 1'b0;
reg [AW - 1: 0] raddr1;
wire [AW - 1: 0] raddr1_p1 = raddr1 + 1'b1;
wire raddr1_tc = (raddr1_p1 == DP);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			raddr1 <= #ck2q 0;
		else if (rd1)
			begin
				if (raddr1_tc)
					raddr1 <= #ck2q 0;
				else
					raddr1 <= #ck2q raddr1_p1;
			end
	end
wire [DW - 1: 0] rdat1;
dpram u_dpram_1(
          .wr ( rd0_d0 ),
          .wclk ( clk ),
          .wdat ( rdat0 ),
          .waddr ( waddr1 ),
          .rd ( rd1 ),
          .rclk ( clk ),
          .rdat ( rdat1 ),
          .raddr ( raddr1 )
      );
//================= line 2 ====================
reg rd1_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			rd1_d0 <= #ck2q 0;
		else
			rd1_d0 <= #ck2q rd1;
	end
reg [AW - 1: 0] waddr2;
wire [AW - 1: 0] waddr2_p1 = waddr2 + 1'b1;
wire waddr2_tc = (waddr2_p1 == DP);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			waddr2 <= #ck2q 0;
		else if (rd1_d0)
			begin
				if (waddr2_tc)
					waddr2 <= #ck2q 0;
				else
					waddr2 <= #ck2q waddr2_p1;
			end
	end
reg full2_ed;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			full2_ed <= #ck2q 0;
		else if (waddr2_tc)
			full2_ed <= #ck2q 1'b1;
	end
wire rd2 = ie ? full2_ed : 1'b0;
reg [AW - 1: 0] raddr2;
wire [AW - 1: 0] raddr2_p1 = raddr2 + 1'b1;
wire raddr2_tc = (raddr2_p1 == DP);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			raddr2 <= #ck2q 0;
		else if (rd2)
			begin
				if (raddr2_tc)
					raddr2 <= #ck2q 0;
				else
					raddr2 <= #ck2q raddr2_p1;
			end
	end
wire [DW - 1: 0] rdat2;
dpram u_dpram_2(
          .wr ( rd1_d0 ),
          .wclk ( clk ),
          .wdat ( rdat1 ),
          .waddr ( waddr2 ),
          .rd ( rd2 ),
          .rclk ( clk ),
          .rdat ( rdat2 ),
          .raddr ( raddr2 )
      );
reg oe;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			oe <= #ck2q 0;
		else
			oe <= #ck2q rd2;
	end
reg [DW - 1: 0]odat0;
reg [DW - 1: 0]rdat0_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			begin
				odat0 <= #ck2q 0;
				rdat0_d0 <= #ck2q 0;
			end
		else
			begin
				odat0 <= #ck2q rdat0_d0;
				rdat0_d0 <= #ck2q rdat0;
			end
	end
reg [DW - 1: 0]odat1;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			odat1 <= #ck2q 0;
		else
			odat1 <= #ck2q rdat1;
	end
assign odat2 = rdat2;
endmodule
