`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/02 20:40:07
// Design Name:
// Module Name: gray_shift
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module gray_shift(clk,
                  resetb,
                  clken,
                  ivsync,
                  ihsync, graya,
                  grayb,
                  oe,
                  sdr_rd,
                  ogray

                 );
input clk;
input resetb;
input clken;
input ivsync;
input ihsync;
input [7 : 0] graya; //next
input [7 : 0] grayb; //before
output oe;
output sdr_rd;
output [15: 0] ogray;
parameter ck2q = 1;
reg oe;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			oe <= #ck2q 0;
		else
			oe <= #ck2q (clken & ivsync & ihsync);
	end
reg ivsync_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			ivsync_d0 <= #ck2q 0;
		else
			ivsync_d0 <= #ck2q ivsync;
	end
reg rd_en;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			rd_en <= #ck2q 0;
		else if (~ivsync & ivsync_d0)
			rd_en <= #ck2q 1'b1;
	end
assign sdr_rd = rd_en & clken;
reg [7: 0] graya_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			graya_d0 <= #ck2q 0;
		else
			graya_d0 <= #ck2q graya;
	end
assign ogray = oe ? {graya_d0, grayb} : 16'd0;
endmodule
