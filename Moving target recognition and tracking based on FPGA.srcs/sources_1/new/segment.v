`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/02 17:09:44
// Design Name:
// Module Name: segment
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module segment(
           clk, resetb,
           cmd,
           cdat,
           x_pos, y_pos,
           sel_seg,
           HEX0, HEX1, HEX2, HEX3, HEX4, HEX5
       );
input clk, resetb;
input cmd;
input [9: 0] cdat;
input [10: 0] x_pos, y_pos;
input sel_seg;
output [6: 0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
parameter ck2q = 1;

reg [7: 0] T_DAT;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			T_DAT <= #ck2q 8'd40;
		else if (cmd & (cdat[9: 8] == 2'd2))
			T_DAT <= #ck2q cdat[7: 0];
	end

reg [31: 0] dat0, dat1, dat2, dat3, dat4, dat5;
always@( * )
	begin
		if (sel_seg)
			begin
				dat5 = 4'd10;
				dat4 = 4'd10;
				dat3 = 4'd10;
				dat2 = T_DAT / 100;
				dat1 = (T_DAT % 100) / 10;
				dat0 = T_DAT % 10;
			end
		else
			begin
				dat5 = x_pos / 100;
				dat4 = (x_pos & 100) / 10;
				dat3 = x_pos % 10;
				dat2 = y_pos / 100;
				dat1 = (y_pos % 100) / 10;
				dat0 = y_pos % 10;
			end
	end

reg [6: 0] HEX5;
always@( * )
	begin
		case (dat5[3: 0])
			4'd0:
				HEX5 = 7'b1000000;
			4'd1:
				HEX5 = 7'b1111001;
			4'd2:
				HEX5 = 7'b0100100;
			4'd3:
				HEX5 = 7'b0110000;
			4'd4:
				HEX5 = 7'b0011001;
			4'd5:
				HEX5 = 7'b0010010;
			4'd6:
				HEX5 = 7'b0000010;
			4'd7:
				HEX5 = 7'b1111000;
			4'd8:
				HEX5 = 7'b0000000;
			4'd9:
				HEX5 = 7'b0011000;
			default:
				HEX5 = 7'b1111111;
		endcase
	end
reg [6: 0] HEX4;
always@( * )
	begin
		case (dat4[3: 0])
			4'd0:
				HEX4 = 7'b1000000;
			4'd1:
				HEX4 = 7'b1111001;
			4'd2:
				HEX4 = 7'b0100100;
			4'd3:
				HEX4 = 7'b0110000;
			4'd4:
				HEX4 = 7'b0011001;
			4'd5:
				HEX4 = 7'b0010010;
			4'd6:
				HEX4 = 7'b0000010;
			4'd7:
				HEX4 = 7'b1111000;
			4'd8:
				HEX4 = 7'b0000000;
			4'd9:
				HEX4 = 7'b0011000;
			default:
				HEX4 = 7'b1111111;
		endcase
	end
reg [6: 0] HEX3;
always@( * )
	begin
		case (dat3[3: 0])
			4'd0:
				HEX3 = 7'b1000000;
			4'd1:
				HEX3 = 7'b1111001;
			4'd2:
				HEX3 = 7'b0100100;
			4'd3:
				HEX3 = 7'b0110000;
			4'd4:
				HEX3 = 7'b0011001;
			4'd5:
				HEX3 = 7'b0010010;
			4'd6:
				HEX3 = 7'b0000010;
			4'd7:
				HEX3 = 7'b1111000;
			4'd8:
				HEX3 = 7'b0000000;
			4'd9:
				HEX3 = 7'b0011000;
			default:
				HEX3 = 7'b1111111;
		endcase
	end
reg [6: 0] HEX2;
always@( * )
	begin
		case (dat2[3: 0])
			4'd0:
				HEX2 = 7'b1000000;
			4'd1:
				HEX2 = 7'b1111001;
			4'd2:
				HEX2 = 7'b0100100;
			4'd3:
				HEX2 = 7'b0110000;
			4'd4:
				HEX2 = 7'b0011001;
			4'd5:
				HEX2 = 7'b0010010;
			4'd6:
				HEX2 = 7'b0000010;
			4'd7:
				HEX2 = 7'b1111000;
			4'd8:
				HEX2 = 7'b0000000;
			4'd9:
				HEX2 = 7'b0011000;
			default:
				HEX2 = 7'b1111111;
		endcase
	end
reg [6: 0] HEX1;
always@( * )
	begin
		case (dat1[3: 0])
			4'd0:
				HEX1 = 7'b1000000;
			4'd1:
				HEX1 = 7'b1111001;
			4'd2:
				HEX1 = 7'b0100100;
			4'd3:
				HEX1 = 7'b0110000;
			4'd4:
				HEX1 = 7'b0011001;
			4'd5:
				HEX1 = 7'b0010010;
			4'd6:
				HEX1 = 7'b0000010;
			4'd7:
				HEX1 = 7'b1111000;
			4'd8:
				HEX1 = 7'b0000000;
			4'd9:
				HEX1 = 7'b0011000;
			default:
				HEX1 = 7'b1111111;
		endcase
	end
reg [6: 0] HEX0;
always@( * )
	begin
		case (dat0[3: 0])
			4'd0:
				HEX0 = 7'b1000000;
			4'd1:
				HEX0 = 7'b1111001;
			4'd2:
				HEX0 = 7'b0100100;
			4'd3:
				HEX0 = 7'b0110000;
			4'd4:
				HEX0 = 7'b0011001;
			4'd5:
				HEX0 = 7'b0010010;
			4'd6:
				HEX0 = 7'b0000010;
			4'd7:
				HEX0 = 7'b1111000;
			4'd8:
				HEX0 = 7'b0000000;
			4'd9:
				HEX0 = 7'b0011000;
			default:
				HEX0 = 7'b1111111;
		endcase
	end

endmodule
