`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/02/26 15:48:42
// Design Name:
// Module Name: i2c
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module i2c(
           sys_clk,
           rst,
           scl,
           sda,
           finish
       );

input wire sys_clk;
input wire rst;
output scl;
inout sda;
output finish;

wire [8: 0] wire_addr;
wire [15: 0] wire_data;

i2c_controller i2c(
                   .sys_clk(sys_clk),
                   .rst(rst),
                   .finish(finish),
                   .scl(scl),
                   .sda(sda),
                   .addr(wire_addr),
                   .data_in(wire_data)
               );

iic_rom rom(
            .address(wire_addr),
            .clock(sys_clk),
            .q(wire_data)
        );
endmodule
