`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/02/26 15:44:09
// Design Name:
// Module Name: spram_cmd
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module spram_cmd(
           clk,
           ie,
           idat,
           oe,
           odat,
           addr

       );
parameter DW = 8;
parameter AW = 4;
parameter DP = 1 << AW;
input clk;
input ie;
input [DW - 1: 0] idat;
input oe;
output [DW - 1: 0] odat;
input [AW - 1: 0] addr;
reg [DW - 1: 0] buffer [0: DP - 1];

always@(posedge clk)
	begin
		if (ie)
			buffer[addr] <= idat;
	end

reg [DW - 1: 0] odat;
always@(posedge clk)
	begin
		if (oe)
			odat <= buffer[addr];
		else
			odat <= 0;
	end

endmodule
