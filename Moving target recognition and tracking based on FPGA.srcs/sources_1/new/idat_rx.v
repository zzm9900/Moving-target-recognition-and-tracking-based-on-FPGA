`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/02 20:47:34
// Design Name:
// Module Name: idat_rx
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module idat_rx(clk,
               resetb,
               idat,
               odat,
               ivsync,
               ihsync,
               ovsync,
               ohsync,
               oclken,
               mode

              );
input clk;
input resetb;
input [7 : 0] idat;
output [15: 0] odat;
input ivsync;
input ihsync;
output ovsync;
output ohsync;
output oclken;
input mode;
parameter ck2q = 1;
reg ivsync_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			ivsync_d0 <= #ck2q 1'b1;
		else
			ivsync_d0 <= #ck2q ivsync;
	end
wire ivsync_ne = ~ivsync & ivsync_d0;
reg mode_r;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			mode_r <= #ck2q 0;
		else if (ivsync_ne)
			mode_r <= #ck2q mode;
	end
reg ivsync_1;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			ivsync_1 <= #ck2q 0;
		else if (ivsync_ne)
			ivsync_1 <= #ck2q ~ivsync_1;
	end
wire ivsync_0 = ivsync & ivsync_1;
wire ivsync_w = mode_r ? ivsync : ivsync_0;
reg [7: 0] idat_d0;
reg ohsync;
reg ovsync;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			begin
				idat_d0 <= #ck2q 0;
				ohsync <= #ck2q 0;
				ovsync <= #ck2q 0;
			end
		else
			begin
				idat_d0 <= #ck2q idat;
				ohsync <= #ck2q ihsync;
				ovsync <= #ck2q ivsync_w;
			end
	end
reg oclken;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			oclken <= #ck2q 0;
		else if (ovsync & ohsync)
			oclken <= #ck2q ~oclken;
		else
			oclken <= #ck2q 0;
	end
reg [15: 0] odat;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			odat <= #ck2q 0;
		else if (~oclken)
			odat <= #ck2q {idat_d0, idat};
	end
/*
reg [15:0] odat;
always@(posedge clk or negedge resetb)begin
 if(!resetb)
 odat <= #ck2q 0;
 else if(ovsync & ohsync & ~oclken)
 odat <= #ck2q odat + 1'b1;
 else if(~ovsync)
 odat <= #ck2q 0;
end
*/
endmodule
