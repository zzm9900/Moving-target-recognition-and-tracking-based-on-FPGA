`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/02/26 15:40:55
// Design Name:
// Module Name: ps2_kb_top
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module ps2_kb_top(
           clk,
           resetb,
           rxc,
           rxd,
           oe,
           odat

       );
input clk;
input resetb;
input rxc;
input rxd;
output oe;
output [7: 0] odat;

wire en_w;
wire [7: 0] dat_w;
ps2_kb_rx u_ps2_kb_rx(
              .clk ( clk ),
              .resetb ( resetb ),
              .rxc ( rxc ),
              .rxd ( rxd ),
              .oe ( en_w ),
              .odat ( dat_w )
          );

ps2_kb_filler u_ps2_kb_filler(
                  .clk ( clk ),
                  .resetb ( resetb ),
                  .ie ( en_w ),
                  .idat ( dat_w ),
                  .oe ( oe ),
                  .odat ( odat )
              );


endmodule
