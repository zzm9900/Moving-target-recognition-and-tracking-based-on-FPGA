`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/02 20:02:56
// Design Name:
// Module Name: colorbar
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module colorbar(
           clk,
           resetb,
           rd,
           rdat
       );

parameter H_DISP = 10'd800;
parameter V_DISP = 10'd600;
parameter ck2q = 1;

input clk;
input resetb;
input rd;
output [23: 0] rdat;

reg [9: 0] hcnt;
wire hcnt_tc = (hcnt == H_DISP - 1);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			hcnt <= #ck2q 0;
		else if (rd)
			begin
				if (hcnt_tc)
					hcnt <= #ck2q 0;
				else
					hcnt <= #ck2q hcnt + 1'b1;
			end
	end

reg [9: 0] vcnt;
wire vcnt_tc = (vcnt == V_DISP - 1);
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			vcnt <= #ck2q 0;
		else if (rd & hcnt_tc)
			begin
				if (vcnt_tc)
					vcnt <= #ck2q 0;
				else
					vcnt <= #ck2q vcnt + 1'b1;
			end
	end
reg [23: 0] rdat;
always@( * )
	begin
		if (vcnt <= 200)
			rdat = 24'hff0000;
		else if (vcnt <= 300)
			rdat = 24'h00ff00;
		else if (vcnt <= 400)
			rdat <= 24'h0000ff;
		else if (vcnt < 480)
			rdat = 24'h00ffff;
		else
			rdat = 0;
	end
endmodule
