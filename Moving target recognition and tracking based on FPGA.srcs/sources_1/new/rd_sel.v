`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/02 20:13:43
// Design Name:
// Module Name: rd_sel
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module rd_sel(
           clk,
           resetb,
           rd,
           isel,
           osel
       );
parameter CNT_MAX = 800 * 600;
parameter ck2q = 1;
input clk;
input resetb;
input rd;
input isel;
output osel;

reg [18: 0] cnt;
wire [18: 0] cnt_p1 = cnt + 1'b1;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			cnt <= #ck2q 0;
		else if (rd)
			begin
				if (cnt_p1 == CNT_MAX)
					cnt <= #ck2q 0;
				else
					cnt <= #ck2q cnt_p1;
			end
	end
reg isel_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			isel_d0 <= #ck2q 0;
		else
			isel_d0 <= #ck2q isel;
	end
reg osel;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			osel <= #ck2q 0;
		else if (cnt_p1 == CNT_MAX)
			osel <= #ck2q isel_d0;
	end
endmodule
