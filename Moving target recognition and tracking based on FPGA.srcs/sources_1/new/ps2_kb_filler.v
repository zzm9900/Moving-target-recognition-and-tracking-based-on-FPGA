`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/02/26 12:19:41
// Design Name:
// Module Name: ps2_kb_filler
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module ps2_kb_filler(
           clk,
           resetb,
           ie,
           idat,
           oe,
           odat
       );

localparam A = 8'h1c;
localparam B = 8'h32;
localparam C = 8'h21;
localparam D = 8'h23;
localparam E = 8'h24;
localparam F = 8'h28;
localparam G = 8'h34;
localparam H = 8'h33;
localparam I = 8'h43;
localparam J = 8'h3b;
localparam K = 8'h42;
localparam L = 8'h4b;
localparam M = 8'h3a;
localparam N = 8'h31;
localparam O = 8'h44;
localparam P = 8'h4d;
localparam Q = 8'h15;
localparam R = 8'h2d;
localparam S = 8'h1b;
localparam T = 8'h2c;
localparam U = 8'h3c;
localparam V = 8'h2a;
localparam W = 8'h1d;
localparam X = 8'h22;
localparam Y = 8'h35;
localparam Z = 8'h1a;
localparam _0 = 8'h45;
localparam _1 = 8'h16;
localparam _2 = 8'h1e;
localparam _3 = 8'h26;
localparam _4 = 8'h25;
localparam _5 = 8'h2e;
localparam _6 = 8'h36;
localparam _7 = 8'h3d;
localparam _8 = 8'h3E;
localparam _9 = 8'h46;
localparam BKSP = 8'h66;
localparam SPACE = 8'h29;
localparam ENTER = 8'h5A;
localparam BREAK = 8'hF0;

input clk;
input resetb;
input ie;
input [7: 0] idat;
output oe;
output [7: 0] odat;
parameter ck2q = 1;
reg ie_d0;
always @(posedge clk or negedge resetb)
	begin
		if (!resetb)
			ie_d0 <= #ck2q 0;
		else
			ie_d0 <= ie;
	end

reg brk;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			brk <= #ck2q 0;
		else
			brk <= #ck2q (idat == BREAK);
	end

reg brk_d0;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			brk_d0 <= #ck2q 0;
		else
			brk_d0 <= #ck2q brk;
	end

reg oe;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			oe <= #ck2q 0;
		else
			oe <= #ck2q ie_d0 & ~(brk | brk_d0);
	end

reg [7: 0] odat;
always@(posedge clk or negedge resetb)
	begin
		if (!resetb)
			odat <= #ck2q 0;
		else if (ie_d0)
			begin
				case (idat)
					A:
						odat <= #ck2q 8'd1;
					B:
						odat <= #ck2q 8'd2;
					C:
						odat <= #ck2q 8'd3;
					D:
						odat <= #ck2q 8'd4;
					E:
						odat <= #ck2q 8'd5;
					F:
						odat <= #ck2q 8'd6;
					G:
						odat <= #ck2q 8'd7;
					H:
						odat <= #ck2q 8'd8;
					I:
						odat <= #ck2q 8'd9;
					J:
						odat <= #ck2q 8'd10;
					K:
						odat <= #ck2q 8'd11;
					L:
						odat <= #ck2q 8'd12;
					M:
						odat <= #ck2q 8'd13;
					N:
						odat <= #ck2q 8'd14;
					O:
						odat <= #ck2q 8'd15;
					P:
						odat <= #ck2q 8'd16;
					Q:
						odat <= #ck2q 8'd17;
					R:
						odat <= #ck2q 8'd18;
					S:
						odat <= #ck2q 8'd19;
					T:
						odat <= #ck2q 8'd20;
					U:
						odat <= #ck2q 8'd21;
					V:
						odat <= #ck2q 8'd22;
					W:
						odat <= #ck2q 8'd23;
					X:
						odat <= #ck2q 8'd24;
					Y:
						odat <= #ck2q 8'd25;
					Z:
						odat <= #ck2q 8'd26;
					_0:
						odat <= #ck2q 8'd27;
					_1:
						odat <= #ck2q 8'd28;
					_2:
						odat <= #ck2q 8'd29;
					_3:
						odat <= #ck2q 8'd30;
					_4:
						odat <= #ck2q 8'd31;
					_5:
						odat <= #ck2q 8'd32;
					_6:
						odat <= #ck2q 8'd33;
					_7:
						odat <= #ck2q 8'd34;
					_8:
						odat <= #ck2q 8'd35;
					_9:
						odat <= #ck2q 8'd36;
					BKSP:
						odat <= #ck2q 8'd37;
					SPACE:
						odat <= #ck2q 8'd38;
					ENTER:
						odat <= #ck2q 8'd39;
					default:
						odat <= #ck2q idat;
				endcase
			end
	end

endmodule
