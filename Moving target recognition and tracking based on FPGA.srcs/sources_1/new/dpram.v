`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/03/02 20:25:11
// Design Name:
// Module Name: dpram
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module dpram(
           wr,
           wclk,
           wdat,
           rd,
           rclk,
           rdat,
           raddr,waddr
       );
parameter DW = 16;
parameter AW = 10;
parameter DP = 10'd640; // DP = 2^10 = 1024
input wr;
input wclk;
input [DW - 1: 0] wdat;
input [AW - 1: 0] waddr;
input rd;
input rclk;
input [AW - 1: 0] raddr;
output [DW - 1: 0] rdat;
parameter ck2q = 1;
//
///////////////////////////////////////////////////////////////////////////////
//
reg [DW - 1: 0] buffer [0: DP - 1];
always@(posedge wclk)
	begin
		if (wr)
			buffer[waddr] <= #ck2q wdat;
	end
reg [DW - 1: 0] rdat;
always@(posedge rclk)
	begin
		if (rd)
			rdat <= #ck2q buffer[raddr];
	end
endmodule

