`timescale 1ns / 1ps 
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/02/26 12:14:54
// Design Name:
// Module Name: controller
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module controller(
           clk,
           resetb,
           rxc,
           rxd,
           oe, odat
       );

input clk;
input resetb;
input rxc;
input rxd;
output oe;
output [9: 0] odat;
wire en_w;
wire [7: 0] dat_w;

ps2_kb_top u_kb(
               .clk(clk),
               .resetb(resetb),
               .rxc(rxc),
               .rxd(rxd),
               .oe(en_w),
               .odat(dat_w)
           );

cmd_coder u_cmd(
              .clk ( clk ),
              .resetb ( resetb ),
              .ie ( en_w ),
              .idat ( dat_w ),
              .oe ( oe ),
              .cmd ( odat[9: 8] ),
              .odat ( odat[7: 0] )
          );

endmodule
